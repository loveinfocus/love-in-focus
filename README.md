Wedding photographer in Bristol, but also covering Bath and around the South west. Hi, I'm Sandy and I'm great at making people relax around cameras.

Address: 119 Beechen Dr, Bristol BS16 4BX, UK

Phone: +44 7876 041154

Website: https://love-in-focus.co.uk/
